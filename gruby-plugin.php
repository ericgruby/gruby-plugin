<?php
/**
* Plugin Name: Gruby Plugin
* Text Domain: gruby_plugin
* Description: Funcionalidades adicionais para o site.
* Version: 1.0
* Author: Eric Gruby
* Author URI: http://ericgruby.com.br
*/

// Defines
define("GRUBY_ABS_PATH", plugin_dir_path( __FILE__ ));
define("GRUBY_LOGS", true);

//Core
foreach( glob(dirname(__FILE__) . '/inc/core/*.php') as $class_path ){
	require_once $class_path ;
}

//Config
foreach( glob(dirname(__FILE__) . '/inc/config/*.php') as $class_path ){
	require_once $class_path ;
}

//Ajax
foreach( glob(dirname(__FILE__) . '/inc/ajax/*/*.php') as $class_path ){
	require_once $class_path ;
}

//Shortcodes
foreach( glob(dirname(__FILE__) . '/inc/shortcodes/*/*.shortcode.php') as $class_path ){
	require_once $class_path ;
}

//Text domain
function gruby_text_domain() {
  load_plugin_textdomain( 'gruby_plugin', false, basename( dirname( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'gruby_text_domain' );
?>
