var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps');
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    rename = require("gulp-rename");


gulp.task('sass', function () {
  return gulp.src('./css/*/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(rename(function (path) {
      path.basename += ".min";
    }))
    .pipe(sourcemaps.write({includeContent: false}))
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(minifycss({debug: true}))
    .pipe(autoprefixer({
      browsers: ['last 6 versions'],
      cascade: false
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(function (file) {
        return file.base;
    }));
});

gulp.task('minify-css', function(){
  return gulp.src('./css/*/*.css')
  .pipe(minifycss({debug: true}))
});

gulp.task('minify-js', function(){
  return gulp.src('./js/*/*.prod.js')
  .pipe(uglify())
  .pipe(rename(function(opt) {
      opt.basename = opt.basename.replace('.prod', '.min');
      return opt;
    }))
  .pipe(gulp.dest(function (file) {
    return file.base;
  }));
});

gulp.task('default', function(){
  return gulp.watch(['./css/*/*.scss', './css/nort_sass_variables.scss', './css/nort_helpers.scss', './css/*/*.css', './js/*/*.js'], ['sass', 'minify-css', 'minify-js']);
})
