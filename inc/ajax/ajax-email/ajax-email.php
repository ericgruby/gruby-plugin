<?php
add_action('wp_ajax_nopriv_ajax_email', 'ajax_email');
add_action('wp_ajax_ajax_email', 'ajax_email');
function ajax_email() {
	if( ! wp_verify_nonce( $_POST['ajax_email'], 'ajax_email' ) ) {
  	wp_redirect('http://127.0.0.1');
  	die();
	}

  $enviar_para = sanitize_email($_POST['email']);
  $nome = sanitize_text_field($_POST['nome']);

	$email = new G_email();
	$email->set_to($enviar_para);
	$email->set_template("email", ["nome"=>$nome]);
	$email->set_attachments([ABSPATH."wp-content/uploads/2018/03/tabela_de_planos.jpg"]);
	$email->set_subject("{$nome}! E-mail para você :)");

	$log = new Log();

	if( $email->send() ){
		$result['status'] = "ok";
		$result['resposta'] = "E-mail enviado para: {$enviar_para}";
		$log->write("success", "E-mail enviado para: {$enviar_para}");
	}else{
		$result['status'] = "no";
		$result['resposta'] = "Algo deu errado na hora de enviar para: {$enviar_para}";
		$log->write("danger", "Algo deu errado na hora de enviar para: {$enviar_para}");
	}

	wp_send_json($result);
	die();
}
?>
