<?php
//Ajax test action
add_action('wp_ajax_nopriv_ajax_test_action_function', 'ajax_test_action_function');
add_action('wp_ajax_ajax_test_action_function', 'ajax_test_action_function');
function ajax_test_action_function() {
	if( ! wp_verify_nonce( $_POST['ajax_test_nonce'], 'ajax_test_nonce' ) ) {
  	wp_redirect('http://127.0.0.1');
  	die();
	}

  $test = sanitize_text_field($_POST['test']);
  $foo = sanitize_text_field($_POST['foo']);
  $bar = sanitize_text_field($_POST['bar']);

	if(empty($test)){
		$result['status'] = "no";
	  $result['resposta'] = "Digite um teste.";

		wp_send_json($result);
		die();
	}

  $result['status'] = "ok";
  $result['resposta'] = "Você enviou {$test}, {$foo} e {$bar}";

	wp_send_json($result);
	die();
}
?>
