<?php
function gruby_post_types() {

  /* ------------------ */
  /*   TESTE POST TYPE
  /* ------------------ */
  $teste = new G_PostType(
    'Teste',
    'teste'
  );
  $teste->set_labels(
    array(
      'menu_name' => 'Testes'
    )
  );
  $teste->set_arguments(
    array(
      'supports' => array( 'title', 'editor', 'thumbnail' ),
      'menu_icon' => 'dashicons-image-filter',
      'public' => false,
      'publicly_queryable' => false,
    )
  );

  /* ------------------ */
  /*   slides
  /* ------------------ */
  $slides = new G_PostType(
    'Slide',
    'slides'
  );
  $slides->set_labels(
    array(
      'menu_name' => 'Slides'
    )
  );
  $slides->set_arguments(
    array(
      'supports' => array( 'title', 'editor', 'thumbnail' ),
      'menu_icon' => 'dashicons-images-alt',
      'public' => false,
      'publicly_queryable' => false,
    )
  );

}

add_action( 'init', 'gruby_post_types', 1 );
?>
