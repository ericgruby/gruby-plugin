<?php
function gruby_taxonomies() {

  /* ------------------ */
  /*   CATEGORIA DE TESTE
  /* ------------------ */
  $cat_teste = new G_Taxonomy(
    'Categoria de Teste',
    'categoria-de-teste',
    'teste'
  );
  $cat_teste->set_labels(
    array(
      'menu_name' => 'Categorias de Teste'
    )
  );


  /* ------------------ */
  /*   TIPOS DE SLIDE
  /* ------------------ */
  $slide_cat = new G_Taxonomy(
    'Tipo de Slide',
    'tipo-de-slide',
    'slides'
  );
  $slide_cat->set_labels(
    array(
      'menu_name' => 'Tipos de Slide', 'gruby_plugin'
    )
  );

}

add_action( 'init', 'gruby_taxonomies', 1 );
?>
