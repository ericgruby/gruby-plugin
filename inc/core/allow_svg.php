<?php
/* ------------------ */
/*   Allow SVG
/* ------------------ */
function add_file_types_to_uploads($file_types){
    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes );
    return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');

function response_for_svg( $response, $attachment, $meta ) {
	if( $response['mime'] == 'image/svg+xml' && empty( $response['sizes'] ) ) {
		$svg_path = get_attached_file( $attachment->ID );
		$dimensions = get_dimensions( $svg_path );
		$response[ 'sizes' ] = array(
			'full' => array(
			'url' => $response[ 'url' ],
			'width' => $dimensions->width,
			'height' => $dimensions->height,
			'orientation' => $dimensions->width > $dimensions->height ? 'landscape' : 'portrait'
			)
		);
	}
	return $response;
}
add_filter( 'wp_prepare_attachment_for_js', 'response_for_svg', 10, 3 );

function get_dimensions( $svg ) {
	$svg = simplexml_load_file( $svg );
	$attributes = $svg->attributes();
	$width = (string) $attributes->width;
	$height = (string) $attributes->height;
	return (object) array( 'width' => $width, 'height' => $height );
}
?>
