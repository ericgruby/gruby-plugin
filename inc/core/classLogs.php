<?php
class Log {

  public $type;
  public $message;

  public function __construct(){
    return $this;
  }

  public function write($type = "", $message){
    $this->type = $type;
    $this->message = $message;
    $date = date("d/m/Y H:i:s");

    $put = "<div class='log {$this->type}'>[{$date}] {$this->message}</div>";

    $write = file_put_contents(GRUBY_ABS_PATH."inc/logs/gruby_logs.php", $put, FILE_APPEND);

    if($write > 0 && $write != false){
      return true;
    }else{
      return false;
    }

  }

  public function clear() {
    file_put_contents(GRUBY_ABS_PATH."inc/core/logs.php", "");

    if($write > 0 && $write != false){
      return true;
    }else{
      return false;
    }

  }

}
?>
