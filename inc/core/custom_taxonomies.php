<?php
/* ------------------ */
/*   Thanks, Odin  (http://wpod.in/)
/* ------------------ */
class G_Taxonomy {

	protected $labels = array();

	protected $arguments = array();

	public function __construct( $name, $slug, $object_type ) {
		$this->name        = $name;
		$this->slug        = $slug;
		$this->object_type = $object_type;

		add_action( 'init', array( &$this, 'register_taxonomy' ) );
	}

	public function set_labels( $labels = array() ) {
		$this->labels = $labels;
	}

	public function set_arguments( $arguments = array() ) {
		$this->arguments = $arguments;
	}

	protected function labels() {
		$default = array(
			'name'                       => sprintf( '%ss', $this->name ),
			'singular_name'              => sprintf( '%s', $this->name ),
			'add_or_remove_items'        => sprintf( __( 'Add or Remove %ss', 'gruby_plugin' ), $this->name ),
			'view_item'                  => sprintf( __( 'View %s', 'gruby_plugin' ), $this->name ),
			'edit_item'                  => sprintf( __( 'Edit %s', 'gruby_plugin' ), $this->name ),
			'search_items'               => sprintf( __( 'Search %s', 'gruby_plugin' ), $this->name ),
			'update_item'                => sprintf( __( 'Update %s', 'gruby_plugin' ), $this->name ),
			'parent_item'                => sprintf( __( 'Parent %s:', 'gruby_plugin' ), $this->name ),
			'parent_item_colon'          => sprintf( __( 'Parent %s:', 'gruby_plugin' ), $this->name ),
			'menu_name'                  => sprintf( '%ss', $this->name ),
			'add_new_item'               => sprintf( __( 'Add New %s', 'gruby_plugin' ), $this->name ),
			'new_item_name'              => sprintf( __( 'New %s', 'gruby_plugin' ), $this->name ),
			'all_items'                  => sprintf( __( 'All %ss', 'gruby_plugin' ), $this->name ),
			'separate_items_with_commas' => sprintf( __( 'Separate %ss with comma', 'gruby_plugin' ), $this->name ),
			'choose_from_most_used'      => sprintf( __( 'Choose from %ss most used', 'gruby_plugin' ), $this->name )
		);
		return array_merge( $default, $this->labels );
	}

	protected function arguments() {
		$default = array(
			'labels'            => $this->labels(),
			'hierarchical'      => true,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true,
		);
		return array_merge( $default, $this->arguments );
	}

	public function register_taxonomy() {
		register_taxonomy( $this->slug, $this->object_type, $this->arguments() );
	}
}
?>
