<?php

class G_email {


  public function __construct(){
    $this->set_headers();
  }

  public function set_to( $para = null ) {
		$this->para = $para;
  }

  public function set_subject( $assunto = "[Mensagem via site]" ) {
		$this->assunto = $assunto;
  }

  public function set_template( $template = null, $contents = array() ) {

    ob_start();
    include GRUBY_ABS_PATH . "inc/email_templates/" . $template . ".php";
    $content = ob_get_contents();
    ob_end_clean();

    $this->template = minify_html($content);
  }

  public function get_template(){
    return $this->template;
  }

  public function set_attachments( $attachments = array() ) {
		$this->attachments = $attachments;
  }

  public function set_headers( $headers = array() ) {
    $defaults = array("Content-Type: text/html; charset=UTF-8");
		$this->headers = array_merge($defaults, $headers);
  }

  public function send(){
    $email = wp_mail($this->para, $this->assunto, $this->template, $this->headers);
    return $email;
  }

}

?>
