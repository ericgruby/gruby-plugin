<?php
/* ------------------ */
/*   FIX <BR> TAGS
/* ------------------ */
if( !function_exists('wpex_fix_shortcodes') ) {
	function wpex_fix_shortcodes($content){
		$array = array (
			'<p>[' => '[',
			']</p>' => ']',
			']<br />' => ']'
		);
		$content = strtr($content, $array);
		return $content;
	}
	add_filter('the_content', 'wpex_fix_shortcodes');
}
?>
