<?php

function logs_admin_menu() {
  if(GRUBY_LOGS)
	  add_menu_page('Logs', 'Gruby Logs', 'administrator', __FILE__, 'logs_admin_html' , "dashicons-media-text" );
}
add_action('admin_menu', 'logs_admin_menu');

function logs_admin_html() {

  echo "<div class='wrap'><h1>Gruby Logs</h1>";
    echo "<div class='gruby_logs'>";
      echo file_get_contents(GRUBY_ABS_PATH."inc/logs/gruby_logs.php");
    echo "</div>";
  echo "</div>";

}
