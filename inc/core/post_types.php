<?php
/* ------------------ */
/*   Thanks, Odin  (http://wpod.in/)
/* ------------------ */
class G_PostType {

	protected $labels = array();

	protected $arguments = array();

	public function __construct( $name, $slug ) {
		$this->name = $name;
		$this->slug = $slug;
		add_action( 'init', array( &$this, 'register_post_type' ) );
	}

	public function set_labels( $labels = array() ) {
		$this->labels = $labels;
	}

	public function set_arguments( $arguments = array() ) {
		$this->arguments = $arguments;
	}

	protected function labels() {
		$default = array(
			'name'               => sprintf( '%ss', $this->name ),
			'singular_name'      => sprintf( '%s', $this->name ),
			'view_item'          => sprintf( __( 'View %s', 'gruby_plugin' ), $this->name ),
			'edit_item'          => sprintf( __( 'Edit %s', 'gruby_plugin' ), $this->name ),
			'search_items'       => sprintf( __( 'Search %s', 'gruby_plugin' ), $this->name ),
			'update_item'        => sprintf( __( 'Update %s', 'gruby_plugin' ), $this->name ),
			'parent_item_colon'  => sprintf( __( 'Parent %s:', 'gruby_plugin' ), $this->name ),
			'menu_name'          => sprintf( '%ss', $this->name ),
			'add_new'            => __( 'Add New', 'gruby_plugin' ),
			'add_new_item'       => sprintf( __( 'Add New %s', 'gruby_plugin' ), $this->name ),
			'new_item'           => sprintf( __( 'New %s', 'gruby_plugin' ), $this->name ),
			'all_items'          => sprintf( __( 'All %ss', 'gruby_plugin' ), $this->name ),
			'not_found'          => sprintf( __( 'No %s found', 'gruby_plugin' ), $this->name ),
			'not_found_in_trash' => sprintf( __( 'No %s found in Trash', 'gruby_plugin' ), $this->name )
		);
		return array_merge( $default, $this->labels );
	}

	protected function arguments() {
		$default = array(
			'labels'              => $this->labels(),
			'hierarchical'        => false,
			'supports'            => array( 'title', 'editor', 'thumbnail', 'comments', 'revisions' ),
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => false,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'has_archive'         => true,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => true,
			'capability_type'     => 'post'
		);
		return array_merge( $default, $this->arguments );
	}

	public function register_post_type() {
		if( !post_type_exists($this->slug) ){
			register_post_type( $this->slug, $this->arguments() );
		}
	}
}
?>
