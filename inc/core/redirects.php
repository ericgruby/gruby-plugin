<?php
function init_redirects(){
  if( is_page("redirect-this-page") ){
    wp_redirect(site_url());
    die();
  }

}
add_action("template_redirect", "init_redirects");
?>
