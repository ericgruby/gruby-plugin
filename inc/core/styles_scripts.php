<?php
/* ------------------ */
/*   Register scripts/styles Frontend
/* ------------------ */
function gruby_scripts(){
  wp_register_style('gruby_css', plugins_url( '../../css/front/gruby_css.min.css', __FILE__ ), array(), '1.0', 'all');
  wp_enqueue_style('gruby_css');

	wp_register_script('gruby_js',  plugins_url( '../../js/front/gruby_js.min.js', __FILE__ ), array('jquery'), '1.0.0', true);
  wp_enqueue_script('gruby_js');
}
add_action('wp_enqueue_scripts', 'gruby_scripts');


/* ------------------ */
/*   Register scripts/styles Admin
/* ------------------ */
function gruby_scripts_admin(){
  wp_register_style('gruby_css_admin', plugins_url( '../../css/admin/gruby-css_admin.min.css', __FILE__ ), array(), '1.0', 'all');
  wp_enqueue_style('gruby_css_admin');

	wp_register_script('gruby_js_admin',  plugins_url( '../../js/admin/gruby-js_admin.min.js', __FILE__ ), array('jquery'), '1.0.0', true);
  wp_enqueue_script('gruby_js_admin');
}
add_action('admin_enqueue_scripts', 'gruby_scripts_admin');


/* ------------------ */
/*   Register scripts/styles Login
/* ------------------ */
function gruby_scripts_login(){
  wp_register_style('gruby_css_login', plugins_url( '../../css/login/gruby-css_login.min.css', __FILE__ ), array(), '1.0', 'all');
  wp_enqueue_style('gruby_css_login');

	wp_register_script('gruby_js_login',  plugins_url( '../../js/login/gruby-js_login.min.js', __FILE__ ), array('jquery'), '1.0.0', true);
  wp_enqueue_script('gruby_js_login');
}
add_action('login_enqueue_scripts', 'gruby_scripts_login');


/* ------------------ */
/*   Register site variables in JS
/* ------------------ */
function gruby_js_variables(){
  wp_register_script( 'site_variables', esc_url( add_query_arg( array( 'js_global' => 1 ), site_url() ) ) );
  wp_enqueue_script( 'site_variables' );
}
add_action('wp_enqueue_scripts', 'gruby_js_variables');

function javascript_variaveis() {
if ( !isset( $_GET[ 'js_global' ] ) ) return;
$variaveis_javascript = array(
  'ajaxurl' => admin_url('admin-ajax.php'),
  'siteurl' => site_url()
);
$new_array = array();
foreach( $variaveis_javascript as $var => $value ) $new_array[] = esc_js( $var ) . ":\"" . esc_js( $value ) . "\"";
header("Content-type: application/x-javascript");
printf('var %s = {%s};', 'js_global', implode( ',', $new_array ) );
exit;
}
add_action( 'template_redirect', 'javascript_variaveis' );
?>
