<?php
function ajax_email_function($atts){
  $atts = shortcode_atts(array(
    'foo' => 'something',
    'bar' => 'something else',
  ), $atts, 'ajax_test');

  $ajax_email = wp_create_nonce('ajax_email');
  $action = "ajax_email";

  ob_start();

  include "form.template.php";

  $render_shortcode = ob_get_clean();

  return minify_html($render_shortcode);
}
add_shortcode('ajax_mail', 'ajax_email_function');
?>
