<form id='ajax-test' class='ajax-form'>
<label for='test'>Ajax mail</label>

  <p><input type='email' placeholder="E-mail" name='email' value=''></p>
  <p><input type='text' placeholder="Nome" name='nome' value=''></p>

  <!-- Ação do ajax -->
  <input type='hidden' name='action' value='<?php echo $action; ?>'>

  <!-- Nonce para ação ajax -->
  <input type='hidden' name='ajax_email' value='<?php echo $ajax_email; ?>'>

  <!-- Ação que ocorrerá antes do formulário ser enviado -->
  <input type='hidden' name='trigger_action_before' value='before_send_event'>

  <!-- Ação que ocorrerá após o formulário ser enviado -->
  <input type='hidden' name='trigger_action_after' value='alert_result'>

  <!-- O atributo data-alt-text é o texto que o botão assume quando o formulário é enviado -->
  <input type='submit' value='Send test' data-alt-text='Sending test...'>
</form>
<div class='error_message'></div>
