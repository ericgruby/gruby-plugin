<?php
function ajax_test_function($atts){
  $atts = shortcode_atts(array(
    'foo' => 'something',
    'bar' => 'something else',
  ), $atts, 'ajax_test');

  $ajax_test_nonce = wp_create_nonce('ajax_test_nonce');
  $action = "ajax_test_action_function";

  ob_start();

  include "form.template.php";

  $render_shortcode = ob_get_clean();

  return minify_html($render_shortcode);
}
add_shortcode('ajax_test', 'ajax_test_function');
?>
