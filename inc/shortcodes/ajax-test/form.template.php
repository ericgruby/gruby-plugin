<form id='ajax-test' class='ajax-form'>
<label for='test'>Test</label>
  <p><input type='text' name='test' id='test' /></p>
  <input type='hidden' name='foo' value='<?php echo $atts['foo']; ?>' />
  <input type='hidden' name='bar' value='<?php echo $atts['bar']; ?>' />

  <!-- Ação do ajax -->
  <input type='hidden' name='action' value='<?php echo $action; ?>' />

  <!-- Nonce para ação ajax -->
  <input type='hidden' name='ajax_test_nonce' value='<?php echo $ajax_test_nonce; ?>' />

  <!-- Ação que ocorrerá antes do formulário ser enviado -->
  <input type='hidden' name='trigger_action_before' value='before_send_event' />

  <!-- Ação que ocorrerá após o formulário ser enviado -->
  <input type='hidden' name='trigger_action_after' value='alert_result' />

  <!-- O atributo data-alt-text é o texto que o botão assume quando o formulário é enviado -->
  <input type='submit' value='Send test' data-alt-text='Sending test...' />
</form>
<div class='error_message'></div>
