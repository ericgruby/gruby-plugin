<tr>
  <td>
    <h4>

        <a href='<?php the_permalink(); ?>'><?php the_title(); ?>

        <?php if($atts['show_date'] == "true" || $atts['show_author'] == "true") : ?>

          <small>&rarr;
            <?php echo $atts['show_author'] != "true" ? "" : get_the_author(); ?>
            <?php if($atts['show_date'] == "true" && $atts['show_author'] == "true"){ echo " | "; } ?>
            <?php echo $atts['show_date'] != "true" ? "" : get_the_date(); ?>
          </small>

        <?php endif; ?>

        </a>
      </h4>
  </td>

</tr>
