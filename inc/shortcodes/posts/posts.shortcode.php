<?php
function post_list_function($atts){
  $atts = shortcode_atts(Array(
    "post_type"=>"post",
    "category"=>"",
    "tag"=>"",
    "posts_per_page"=>5,
    "orderby"=>'date',
    "show_author"=>'true',
    "show_date"=>'true'
  ), $atts, 'post_list');

  $args = Array(
    "post_type"=>$atts['post_type'],
    "post_status"=>"publish",
    "posts_per_page"=>$atts['posts_per_page']
  );

  if(!empty($atts['category'])){
    $args['category_name'] = $atts['category'];
  }

  if(!empty($atts['orderby'])){
    $args['orderby'] = $atts['orderby'];
  }

  if(!empty($atts['tag'])){
    $args['tag'] = $atts['tag'];
  }

  $list_query = new WP_Query($args);

  ob_start();

  if($list_query->have_posts()){
    while($list_query->have_posts()){
      $list_query->the_post();
      include "post.template.php";
    }
  }else{
    $list = "<h4>Nenhum post encontrado!</h4>";
  }

  $list = "<table class='post_list'>".ob_get_contents()."</table>";

  wp_reset_query();
  ob_end_flush();

  return minify_html($list);
}
add_shortcode('post_list', 'post_list_function');
?>
