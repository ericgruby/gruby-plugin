<?php
function test_function(){

  $args = Array(
    'post_type'=>'teste',
    'post_status'=>'publish',
    'posts_per_page'=> -1
  );

  $query = new WP_Query($args);

  ob_start();

  if($query->have_posts()){
    while($query->have_posts()){
      $query->the_post();
      $title = get_the_title( get_the_ID() );
      include "test.template.php";
    }
  }else{
    $render = "Nothing found";
  }

  wp_reset_query();

  $render = ob_get_contents();

  ob_end_flush();

  return minify_html($render);

}
add_shortcode('test', 'test_function');
?>
