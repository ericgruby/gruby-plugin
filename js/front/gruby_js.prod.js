(function($) {

  console.log('GRUBY JS Front - Ready!');

  //ajax test js
  $(".ajax-form").submit(function(e) {
    e.preventDefault();
    var $this = $(this);
    var data = $this.serialize();
    console.log(data);
    var btn = $('[type="submit"]');
    var btn_val = btn.attr('value');
    var btn_alt = btn.attr('data-alt-text');
    btn.attr('data-orig-text', btn_val);
    var btn_orig = btn.attr('data-orig-text');
    jQuery.ajax({
      type: "post",
      dataType: "json",
      url: js_global.ajaxurl,
      data: data,
      beforeSend: function() {
        btn.attr('value', btn_alt).prop('disabled', true);
        $(document).trigger( $this.find('[name="trigger_action_before"]').val() );
      },
      success: function(response) {
        btn.attr('value', btn_orig).prop('disabled', false);
        $(document).trigger( $this.find('[name="trigger_action_after"]').val(), response );
      }
    })
  });

  $('.ajax-form.pre-charge').submit();

  //Ajax test events
  $(document).on('before_send_event', function(){
    $('.error_message').slideUp().html('');
  });

  $(document).on('alert_result', function(e, response){
    if(response.status == 'ok'){
      alert(response.resposta);
    }else{
      $('.error_message').html(response.resposta).slideDown();
    }
  });

})(jQuery);
