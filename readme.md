# Gruby Plugin

## Módulo para adicionar post types, taxonomies, ajax functions, shortcodes, javascript e CSS.

### Post types

_**/inc/config/config.PostTypes.php**_

(Adaptado do [Odin Framework](http://wpod.in/)) Dentro da função "_gruby_post_types()_", instancie o G_PostType:
```php
/* ------------------ */
/*   POST TYPE
/* ------------------ */
$post_typey = new G_PostType(
  'Post Type', // nome no singular
  'post-type' // slug
);
// Abaixo, são válidos todos os argumentos presentes em https://codex.wordpress.org/Function_Reference/register_post_type#Arguments
// tanto para as Labels quanto para os Arguments
$post_typey->set_labels(
  array(
    'menu_name' => 'Post Types' // nome que aparece no menu
  )
);
$post_typey->set_arguments(
  array(
    'supports' => array( 'title', 'editor', 'thumbnail' ),
    'menu_icon' => 'dashicons-image-filter',
    'public' => false,
    'publicly_queryable' => false,
  )
);
```

### Taxonomies

_**/inc/config/config.Taxonomies.php**_

(Adaptado do [Odin Framework](http://wpod.in/)) Dentro da função "_gruby_taxonomies()_", instancie o G_Taxonomy:
```php
/* ------------------ */
/*   TAXONOMY
/* ------------------ */
$taxonomy = new G_Taxonomy(
  'Categoria de Teste', // nome no singular
  'categoria-de-teste', // slug
  'post-type' // post_type o qual esta taxonomia será vinculada
);
$taxonomy->set_labels(
  array(
    'menu_name' => 'Categorias de Teste' // nome que aparece no menu
  )
);
$taxonomy->set_arguments(
  array(
    'hierarchical' => true
  )
);
```

### Image Sizes

_**inc/config/config.ImageSizes.php**_

Insira os tamanhos de imagem à vontade. Até mesmo imagens menores serão redimensionadas corretamente. Em caso de dúvida, após adicionar um _image_size()_ personalizado, utilize o plugin (Regenerate Thumbnails)[https://br.wordpress.org/plugins/regenerate-thumbnails/]

```php
/* ------------------ */
/*   Exemplo
/* ------------------ */
add_image_size("nome-do-tamanho", (number) largura, (number) altura, (bol) cortar_imagem);
```


### Shortcode

_**inc/shortcodes/shortcode_name/shortcode_name.shortcode.php**_

Adicione a extensão ".shortcode.php" para o arquivo que contém o script do shortcode. A função de autoinsert de arquivos buscará os arquivos com essa extensão.<br>Utilize o padrão ".template.php" para os includes dentro do shortcode. Veja os shortcodes já criados para entender melhor.

### Ajax functions

_**inc/ajax/ajax_function/ajax_function.php**_

### Gulp.js

```
npm install
```
Aguarde a instalação das packages e simplesmente execute:
```
gulp
```

### CSS

Dividido em 3 partes (admin, front e login). Edite os arquivos .scss, e, se o Gulp estiver rodando, você verá que a compilação estará automatizada.

As variáveis são utilizadas em todas as divisões &rarr; _css/nort_sass_variables.scss_

### JS

Dividido em 3 partes (admin, front e login). Edite os arquivos .prod.js, e, se o Gulp estiver rodando, você verá que os arquivos serão minimizados automaticamente.

### Tarefas

- [x] Implementar Gulp
- [x] Implementar OOP
- [x] Implementar Tradução
- [x] Implementar Image Sizes
- [ ] Mecanismo de envio de e-mails com templates responsivos
